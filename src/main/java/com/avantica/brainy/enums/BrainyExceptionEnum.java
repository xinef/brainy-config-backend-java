package com.avantica.brainy.enums;

public enum BrainyExceptionEnum {

    GENERAL_EXCEPTION(1, "General exception");

    private int code;
    private String message;

    BrainyExceptionEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

}
