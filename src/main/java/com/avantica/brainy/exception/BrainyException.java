package com.avantica.brainy.exception;

public class BrainyException extends RuntimeException {

    private Integer code;

    public BrainyException(Integer code, String message, Exception e) {
        super(message, e);
        this.code = code;
    }

    public BrainyException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }
}
