package com.avantica.brainy.controller;


import com.avantica.brainy.model.Configuration;
import com.avantica.brainy.model.Message;
import com.avantica.brainy.enums.BrainyExceptionEnum;
import com.avantica.brainy.exception.BrainyException;
import com.avantica.brainy.service.ConfigurationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@CrossOrigin
@RestController
@RequestMapping(value = "/configuration")
public class ConfigurationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationController.class);

    private ConfigurationService configurationService;

    @Autowired
    public ConfigurationController(ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    @RequestMapping(method = PUT)
    public ResponseEntity updateAllConfiguration(@RequestBody List<Configuration> configurationList) {
        LOGGER.info("update configuration");
        configurationService.updateAllConfiguration(configurationList);
        return new ResponseEntity<Configuration>(OK);
    }

    @RequestMapping(method = GET)
    public ResponseEntity<List<Configuration>> getAllConfigurations() {
        LOGGER.info("get all configurations");
        List<Configuration> listResult = configurationService.getAllConfigurations();
        return new ResponseEntity<>(listResult, OK);
    }

    @ExceptionHandler({BrainyException.class, Exception.class})
    public ResponseEntity<Message> handleGenericError(HttpServletRequest req, Exception ex) {
        LOGGER.error("BRAINY_EX: page:" + req.getRequestURL() + ", ex:" + ex);
        String generalErrorMessage = BrainyExceptionEnum.GENERAL_EXCEPTION.getMessage();
        return new ResponseEntity<>(new Message(generalErrorMessage), INTERNAL_SERVER_ERROR);
    }

}
