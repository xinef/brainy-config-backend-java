package com.avantica.brainy.model;


public class Message{

    private String description;

    public Message(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return String.format("Message{description='%s'}", description);
    }
}