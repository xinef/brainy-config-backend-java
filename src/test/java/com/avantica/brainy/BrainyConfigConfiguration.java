package com.avantica.brainy;

import com.avantica.brainy.custommocks.ConfigurationContainer;
import com.avantica.brainy.custommocks.InMemoryConfigurationDAO;
import com.avantica.brainy.dao.ConfigurationDAO;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BrainyConfigConfiguration {

    private ConfigurationContainer configurationContainer = new ConfigurationContainer();

    @Bean
    public ConfigurationContainer configurationContainer() {
        return configurationContainer;
    }


    @Bean
    public ConfigurationDAO configurationDAO() {
        return new InMemoryConfigurationDAO(configurationContainer);
    }
}
