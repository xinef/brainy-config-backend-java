package com.avantica.brainy.controller;


import com.avantica.brainy.Application;
import com.avantica.brainy.BrainyConfigConfiguration;
import com.avantica.brainy.builders.ConfigurationBuilder;
import com.avantica.brainy.custommocks.ConfigurationContainer;
import com.avantica.brainy.exception.BrainyException;
import com.avantica.brainy.model.Configuration;
import com.avantica.brainy.service.ConfigurationService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DelegatingSmartContextLoader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = DelegatingSmartContextLoader.class, classes = {Application.class, BrainyConfigConfiguration.class})
public class ConfigurationControllerTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Autowired
    private ConfigurationController configurationController;
    @Autowired
    private ConfigurationContainer configurationContainer;
    @Mock
    private ConfigurationService configurationService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        configurationContainer.removeAll();
    }

    @Test
    public void shouldRetrieveAnEmptyListWhenGettingAllConfigurationsTheFirstTime() {
        ResponseEntity<List<Configuration>> responseEntity = configurationController.getAllConfigurations();

        List<Configuration> configurationList = responseEntity.getBody();

        assertThat(configurationList.size(), is(0));
    }

    @Test
    public void shouldRetrieveThePassedElementsWhenGettingAllConfigurationsAfterUpdateWithNonEmptyList() {
        configurationController.updateAllConfiguration(generateConfigurations());

        ResponseEntity<List<Configuration>> responseEntity = configurationController.getAllConfigurations();

        List<Configuration> configurationList = responseEntity.getBody();

        assertThat(configurationList.size(), is(3));
    }

    @Test
    public void shouldRetrieveAnEmptyListWhenGettingAllConfigurationsAfterUpdateWithEmptyList() {
        configurationController.updateAllConfiguration(new ArrayList<>());

        ResponseEntity<List<Configuration>> responseEntity = configurationController.getAllConfigurations();

        List<Configuration> configurationList = responseEntity.getBody();

        assertThat(configurationList.size(), is(0));
    }

    @Test
    public void shouldThrowABrainyExceptionWhenUpdatingConfigurationsAndTheDatasourceIsNotAvailable() {
        expectedException.expect(BrainyException.class);
        configurationController = new ConfigurationController(configurationService);

        when(configurationService.updateAllConfiguration(anyList())).thenThrow(BrainyException.class);

        configurationController.updateAllConfiguration(new ArrayList<>());
    }

    @Test
    public void shouldRetrieveACodeSuccessStatusWhenUpdatingConfigurations() throws Exception {
        List<Configuration> configurationList = generateConfigurations();

        ResponseEntity responseEntity = configurationController.updateAllConfiguration(configurationList);

        HttpStatus httpStatus = responseEntity.getStatusCode();

        assertThat(httpStatus, is(HttpStatus.OK));
    }

    private List<Configuration> generateConfigurations() {
        return Arrays.asList(new ConfigurationBuilder().build(), new ConfigurationBuilder().build(), new ConfigurationBuilder().build());
    }
}
