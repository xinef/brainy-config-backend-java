package com.avantica.brainy.custommocks;

import com.avantica.brainy.model.Configuration;
import com.avantica.brainy.dao.ConfigurationDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class InMemoryConfigurationDAO implements ConfigurationDAO {

    private final ConfigurationContainer configurationContainer;

    @Autowired
    public InMemoryConfigurationDAO(ConfigurationContainer configurationContainer) {
        this.configurationContainer = configurationContainer;
    }

    @Override
    public boolean updateAllConfiguration(List<Configuration> configurationList) {
        return configurationContainer.updateAll(configurationList);

    }

    @Override
    public List<Configuration> getAllConfigurations() {
        return configurationContainer.getAll();
    }
}
