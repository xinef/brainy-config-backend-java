Brainy Config API
=================

Brainy Config API is a REST service that helps to store the common configuration parameters for a set of microservices.
All those parameters will be read by the microservices when launched and when they got changed.
For that, the microservices should listen to the change events on this API so they can receive the latest configuration values.
This API can be configured directly via the REST endpoints or using a client like the [Brainy Config Web](https://gitlab.com/xinef/brainy-config-frontend).

How to run with docker
----------------------

1. Open a bash terminal with a docker machine running.
> That is:
> 1. In Linux just open the terminal.
> 1. In Mac and Windows open the "Docker Quick Start Terminal".

1. Create the docker image and container with the `Dockerfile` provided on this project (execute it on the project root folder).
```console
. docker-create-local-image.sh
```

1. Then in the same folder create your docker container based on the image created on the previous step.
```console
. docker-start-local-container.sh
```
Voilà! You are now inside the created container.

1. Initialize the application.
```console
./gradlew bootRun
```

1. Open http://localhost:8080 (Linux) or http://192.168.99.100:8080 (Mac or Windows) in your favorite browser.
